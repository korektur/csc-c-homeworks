﻿using System;

namespace csc_c_homeworks
{
    public class Card
    {
        private enum Values
        {
            Deuce = 2,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten,
            Jack,
            Queen,
            King,
            Ace
        }

        private enum Suit
        {
            Spades,
            Hearts,
            Diamonds,
            Clubs
        }

        private static readonly Random Random = new Random();

        private readonly Values _value;
        private readonly Suit _suit;

        private Card(Values value, Suit suit)
        {
            _value = value;
            _suit = suit;
        }

        public int GetValue()
        {
            int val = (int) _value;
            if (val == 14) return 11;
            return val >= 10 ? 10 : val;
        }

        public static Card GetRandomCard()
        {
            return new Card((Values) (Random.Next(2, 14)), (Suit) Random.Next(4));
        }

        public override int GetHashCode()
        {
            return (int) _suit*100 + (int) _value;
        }

        public override string ToString()
        {
            return _value + " of " + _suit;
        }

        public override bool Equals(object obj)
        {
            var other = (Card) obj;
            return _value == other._value && _suit == other._suit;
        }
    }
}