﻿using System;

namespace csc_c_homeworks
{
    public abstract class Game
    {
        protected static void PrintUsage()
        {
            Console.WriteLine("To make a bet type sum of bet. It must be an integer number more then 0.");
            Console.WriteLine("To exit to main menu type \"exit\".");
        }

        public abstract void Play(Account account);
    }
}