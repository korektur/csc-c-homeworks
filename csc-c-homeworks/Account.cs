﻿using System;

namespace csc_c_homeworks
{
    public class Account
    {
        private readonly string _name;

        public string Name
        {
            get { return _name; }
        }

        public long CreaditsAmount { get; private set; }

        public Account(string name, long creditsAmount)
        {
            _name = name;
            CreaditsAmount = creditsAmount;
        }

        public void Deposit(long amountToDeposit)
        {
            if (amountToDeposit < 0)
            {
                throw new ArgumentOutOfRangeException("amountToDeposit", "Amount to deposit must be > 0.");
            }
            else if (Int64.MaxValue - CreaditsAmount < amountToDeposit)
            {
                throw new ArgumentOutOfRangeException("amountToDeposit",
                    "Your account total amount cant be more then " + Int64.MaxValue);
            }
            Logger.Log(amountToDeposit + " deposited on " + ToString());
            CreaditsAmount += amountToDeposit;
        }

        public bool Withdraw(long amountToWithdraw)
        {
            if (amountToWithdraw > 0 && CreaditsAmount - amountToWithdraw >= 0)
            {
                CreaditsAmount -= amountToWithdraw;
                Logger.Log(amountToWithdraw + " withdrawed from " + ToString());
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return _name + " " + CreaditsAmount;
        }

        public override int GetHashCode()
        {
            return _name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = (Account) obj;
            return _name == other._name;
        }
    }
}