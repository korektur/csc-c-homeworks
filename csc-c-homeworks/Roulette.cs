﻿using System;

namespace csc_c_homeworks
{
    public class Roulette : Game
    {
        private static readonly Random Random = new Random();

        public override void Play(Account account)
        {
            while (true)
            {
                PrintUsage();
                Console.Write("> ");
                var input = Console.ReadLine();
                long betValue;
                if (long.TryParse(input, out betValue) && account.Withdraw(betValue))
                {
                    Logger.Log("Player bet is " + input);
                    while (true)
                    {
                        Console.WriteLine("To complete a bet choose a number between 1 and 36 " +
                                          "or choose colore (black or red). Then type your choice");
                        Console.Write("> ");
                        input = Console.ReadLine();
                        int chosenSlot;
                        int winFactor = 0;
                        if (int.TryParse(input, out chosenSlot))
                        {
                            if (chosenSlot <= 0 || chosenSlot > 36)
                            {
                                Console.WriteLine("Wrong slot number.");
                                continue;
                            }
                            Logger.Log("Player chose " + chosenSlot);
                            Console.WriteLine("Yo've chosen slot #" + input);
                            var res = Random.Next(1, 37);
                            Console.WriteLine("The result slot is " + res);
                            Logger.Log("Result is " + res);
                            if (res == chosenSlot)
                            {
                                winFactor = 4;
                            }
                        }
                        else if ("red".Equals(input) || "black".Equals(input))
                        {
                            Logger.Log("Player chose " + input);
                            Console.WriteLine("You bet on " + input);
                            var res = Random.Next(1) == 1 ? "red" : "black";
                            Logger.Log("Result color is " + res);
                            Console.WriteLine("Result color is " + res);
                            if (res.Equals(input)) winFactor = 2;
                        }
                        else
                        {
                            Console.WriteLine("Wrong input");
                            continue;
                        }

                        if (winFactor > 0)
                        {
                            Logger.Log("Player won! With bet factor " + winFactor);
                            Console.WriteLine("You won! Congtrats!");
                            try
                            {
                                account.Deposit(betValue*winFactor);
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                Logger.Log("Cant put your prize on your account. Too much money=)");
                                Console.WriteLine("Cant put your prize on your account. Too much money=)");
                                account.Deposit(betValue);
                            }
                        }
                        else
                        {
                            Logger.Log("Player lost");
                            Console.WriteLine("Sorry you lost=(");
                        }
                        Console.WriteLine(account);
                        break;
                    }
                    continue;
                }
                if ("exit".Equals(input))
                {
                    Logger.Log("exiting Roulette.");
                    return;
                }
                Console.WriteLine("Wrong Input. Try again.");
            }
        }
    }
}