﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace csc_c_homeworks
{
    public class Blackjack : Game
    {
        private static int GetHandScore(HashSet<Card> cards)
        {
            return cards.Sum(card => card.GetValue());
        }

        private static string PrintHand(HashSet<Card> cards)
        {
            var hand = new StringBuilder();
            foreach (var card in cards)
            {
                hand.AppendLine(card.ToString());
            }
            return hand.ToString();
        }

        private static void AddCard(HashSet<Card> cards)
        {
            while (!cards.Add(Card.GetRandomCard()))
            {
            }
        }

        public override void Play(Account account)
        {
            while (true)
            {
                PrintUsage();
                Console.Write("> ");
                var input = Console.ReadLine();
                long betValue;
                if (long.TryParse(input, out betValue) && account.Withdraw(betValue))
                {
                    Logger.Log("Player bet is " + input);
                    var playerHand = new HashSet<Card>();
                    while (playerHand.Count < 2)
                    {
                        playerHand.Add(Card.GetRandomCard());
                    }
                    Logger.Log("Player hand is");
                    Logger.Log(PrintHand(playerHand));
                    int playerScore = GetHandScore(playerHand);
                    while (true)
                    {
                        Console.WriteLine("Your hand:");
                        PrintHand(playerHand);

                        Console.WriteLine("Your score is: " + playerScore);
                        Console.WriteLine("Do you need another card? Type \"Yes\" or \"No\"");
                        Console.Write("> ");
                        input = Console.ReadLine();
                        if (input == null) continue;
                        if (input.Equals("Yes"))
                        {
                            AddCard(playerHand);
                            playerScore = GetHandScore(playerHand);
                            Logger.Log("Added one card to player hand. Hand is: ");
                            Logger.Log(PrintHand(playerHand));
                            if (playerScore > 21) break;
                        }
                        else if (input.Equals("No"))
                        {
                            break;
                        }
                        Console.WriteLine("Wrong input.");
                    }

                    Console.WriteLine("Your final hand:");
                    string s = PrintHand(playerHand);
                    Console.Write(s);
                    Logger.Log("Players final hand is: ");
                    Logger.Log(s);
                    Console.WriteLine("Your final score is:" + playerScore);
                    Logger.Log("Players final score is" + playerScore);
                    var casinoHand = new HashSet<Card>();
                    while (casinoHand.Count < 2)
                    {
                        var card = Card.GetRandomCard();
                        if (playerHand.Contains(card) || casinoHand.Contains(card)) continue;
                        casinoHand.Add(card);
                    }

                    int casinoScore = GetHandScore(casinoHand);
                    Console.WriteLine("Casinos hand is:");
                    s = PrintHand(casinoHand);
                    Console.Write(s);
                    Logger.Log("Casinos hand is: ");
                    Logger.Log(s);
                    Console.WriteLine("Casinos final score is: " + casinoScore);
                    Logger.Log("Casinos final score is " + casinoScore);
                    if ((playerScore > casinoScore && playerScore <= 21) || casinoScore > 21)
                    {
                        Logger.Log("Player won!");
                        Console.WriteLine("Congrats! You won!");
                        try
                        {
                            account.Deposit(betValue*2);
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            Console.WriteLine("Cant put your prize on your account. Too much money=)");
                            account.Deposit(betValue);
                        }
                    }
                    else
                    {
                        Logger.Log("Player lost");
                        Console.WriteLine("Sory, you lost.");
                    }
                }
                else if ("exit".Equals(input))
                {
                    Logger.Log("exiting Blackjack");
                    return;
                }
                Console.WriteLine("Wrong Input. Try again.");
            }
        }
    }
}