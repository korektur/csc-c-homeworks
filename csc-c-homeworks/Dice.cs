﻿using System;

namespace csc_c_homeworks
{
    public class Dice : Game
    {
        private static readonly Random Random = new Random();

        public override void Play(Account account)
        {
            while (true)
            {
                PrintUsage();
                Console.Write("> ");
                var input = Console.ReadLine();
                long betValue;
                if (long.TryParse(input, out betValue) && account.Withdraw(betValue))
                {
                    Logger.Log("Players bet is " + betValue);
                    while (true)
                    {
                        Console.WriteLine("To complete a bet choose a number between 2 and 12");
                        Console.Write("> ");
                        input = Console.ReadLine();
                        int chosenSum;
                        if (int.TryParse(input, out chosenSum))
                        {
                            if (chosenSum <= 2 || chosenSum > 12)
                            {
                                Console.WriteLine("Wrong sum. It must be between 2 and 12");
                                continue;
                            }
                            Logger.Log("Player chose " + input);
                            Console.WriteLine("Yo've chosen " + input);
                            var res = Random.Next(2, 13);
                            Logger.Log("Result is" + res);
                            Console.WriteLine("The result slot is " + res);
                            if (res == chosenSum)
                            {
                                Logger.Log("Player won");
                                Console.WriteLine("You won! Congtrats!");
                                try
                                {
                                    account.Deposit(betValue*2);
                                }
                                catch (ArgumentOutOfRangeException)
                                {
                                    Console.WriteLine("Cant put your prize on your account. Too much money=)");
                                    account.Deposit(betValue);
                                }
                            }
                            else
                            {
                                Logger.Log("Player lost");
                                Console.WriteLine("Sorry, you lost.");
                            }
                            Console.WriteLine(account);
                            break;
                        }
                        Console.WriteLine("Wrong input");
                    }
                    continue;
                }
                if ("exit".Equals(input))
                {
                    Logger.Log("exiting Dices");
                    return;
                }
                Console.WriteLine("Wrong Input. Try again.");
            }
        }
    }
}