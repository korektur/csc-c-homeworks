﻿using System;
using System.Collections.Generic;
using System.IO;

namespace csc_c_homeworks
{
    internal class Program
    {
        private static Dictionary<string, Account> GetAccounts()
        {
            var accounts = new Dictionary<string, Account>();
            if (File.Exists("accounts.txt"))
            {
                using (var reader = new StreamReader("accounts.txt", System.Text.Encoding.UTF8))
                {
                    while (!reader.EndOfStream)
                    {
                        var s = reader.ReadLine();
                        // ReSharper disable once PossibleNullReferenceException
                        var index = s.LastIndexOf(' ');
                        var name = s.Substring(0, index);
                        var amount = long.Parse(s.Substring(index + 1));
                        accounts.Add(name, new Account(name, amount));
                    }
                }
            }
            else
            {
                File.Create("accounts.txt").Close();
            }

            Logger.Log("Accounts retrieved.");
            return accounts;
        }

        private static void WriteAccounts(Dictionary<string, Account> accounts)
        {
            using (var writer = new StreamWriter("accounts.txt", false))
            {
                foreach (var account in accounts.Values)
                {
                    writer.WriteLine(account);
                }
            }
            Logger.Log("All accounts saved to file.");
        }

        private static Account ChangeAccount(IDictionary<string, Account> accounts)
        {
            if (accounts == null) throw new ArgumentNullException("accounts");
            string name = null;
            while (string.IsNullOrEmpty(name))
            {
                Console.Write("Enter your name: ");
                name = Console.ReadLine();
            }
            Account account;
            try
            {
                account = accounts[name];
                Logger.Log("Account changed to " + account);
                Console.WriteLine("Welcome again!");
                Console.WriteLine(account + " credits!");
            }
            catch (KeyNotFoundException)
            {
                account = new Account(name, 0);
                accounts.Add(account.Name, account);
                Logger.Log("Created new account " + account);
                Console.WriteLine("You are new here, you need to deposit some " +
                                  "money on your account first, before playing.");
            }

            return account;
        }

        private static void PrintUsage()
        {
            Console.WriteLine("To deposit type \"deposit <amount to deposit>\". Amount must be an integer.");
            Console.WriteLine("To start playing game type \"Blackjack\" or \"Dice\" or \"Roulette\".");
            Console.WriteLine("To exit type \"exit\"");
            Console.WriteLine("To change user type \"chuser\"");
        }

        // ReSharper disable once UnusedParameter.Local
        private static void Main(string[] args)
        {
            Logger.InitLog();
            var accounts = GetAccounts();
            var account = ChangeAccount(accounts);
            Game roulette = new Roulette();
            Game dice = new Dice();
            Game blackJack = new Blackjack();

            while (true)
            {
                PrintUsage();
                Console.Write(">");
                var input = Console.ReadLine();

                if (input == null) return;
                Logger.Log("Player input " + input);
                long toDeposit;

                if (input.StartsWith("deposit ") && long.TryParse(input.Substring("deposit ".Length), out toDeposit))
                {
                    Logger.Log("User " + account + " tries to deposit " + toDeposit);
                    try
                    {
                        account.Deposit(toDeposit);
                        Console.WriteLine("Deposit successful! Your current balance is " + account.CreaditsAmount);
                        Logger.Log("Deposit successful");
                    }
                    catch (ArgumentOutOfRangeException e)
                    {
                        Console.WriteLine(e.Message);
                        Logger.Log(e.Message);
                    }
                }
                else if (input.Equals("exit"))
                {
                    Logger.Log("User " + account + " exited.");
                    WriteAccounts(accounts);
                    break;
                }
                else if (input.Equals("chuser"))
                {
                    Logger.Log("User " + account + " changed user");
                    account = ChangeAccount(accounts);
                }
                else if (input.Equals("Blackjack"))
                {
                    Logger.Log("User " + account + " chosen Blackjack.");
                    blackJack.Play(account);
                }
                else if (input.Equals("Dice"))
                {
                    Logger.Log("User " + account + " chosen Dice.");
                    dice.Play(account);
                }
                else if (input.Equals("Roulette"))
                {
                    Logger.Log("User " + account + " chosen Roulette.");
                    roulette.Play(account);
                }
                else
                {
                    continue;
                }
                WriteAccounts(accounts);
            }
        }
    }
}