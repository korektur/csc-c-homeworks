﻿using System;
using System.IO;

namespace csc_c_homeworks
{
    public class Logger
    {
        public static void InitLog()
        {
            if (!File.Exists("log.txt"))
            {
                File.Create("log.txt").Close();
            }
        }

        public static void Log(String log)
        {
            File.AppendAllText("log.txt", String.Format("[{0} {1}]{2}" + Environment.NewLine,
                DateTime.Now, TimeZone.CurrentTimeZone.StandardName, log));
        }
    }
}